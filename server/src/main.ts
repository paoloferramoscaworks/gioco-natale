import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(
    parseInt(process.env.GIOCO_NATALE_BE_NODE_PORT ?? '3000', 10),
  );
}
bootstrap();
