import { Module } from '@nestjs/common';
import { VersionController } from './version/version.controller';
import { VersionService } from './version/version.service';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(
        __dirname,
        process.env.GIOCO_NATALE_PUBLIC_PATH || '../../public',
      ),
      renderPath: '/',
    }),
  ],
  controllers: [VersionController],
  providers: [VersionService],
})
export class AppModule {}
