import { Injectable } from '@nestjs/common';
import { execSync } from 'child_process';

export declare type VersionDto = {
  package:string
};

@Injectable()
export class VersionService {
  getVersion(): VersionDto {
    return {
      package: execSync("npm ls --depth=-1").toString().replace(/.*([\d]+\.[\d]+\.[\d]+).*$/s, '$1')
    };
  }
}
