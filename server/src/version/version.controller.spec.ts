import { Test, TestingModule } from '@nestjs/testing';
import { VersionController } from './version.controller';
import { VersionService } from './version.service';
import { execSync } from 'child_process';
// import { version } from 'package.json'

describe('VersionController', () => {
  let versionController: VersionController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [VersionController],
      providers: [VersionService],
    }).compile();

    versionController = app.get<VersionController>(VersionController);
  });

  describe('root', () => {
    it('should return the version of the server', () => {
      expect(versionController.getVersion().package).toBe(
        execSync("npm ls --depth=-1").toString().replace(/.*([\d]+\.[\d]+\.[\d]+).*$/s, '$1')
      );
    });
  });
});
