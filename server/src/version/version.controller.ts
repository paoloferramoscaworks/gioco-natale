import { Controller, Get } from '@nestjs/common';
import { VersionDto, VersionService } from './version.service';

@Controller('version')
export class VersionController {
  constructor(private readonly versionService: VersionService) {}

  @Get()
  getVersion(): VersionDto {
    return this.versionService.getVersion();
  }
}
