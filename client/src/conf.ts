import { Scale, Types } from "phaser";
import TileMapScene from "./scenes/tilemap/tilemap.scene";
import Demo from "./scenes/demo.scene";


const conf:Types.Core.GameConfig = {
    mode: Scale.RESIZE,
    height: 256,
    width: 256,
    scene: [TileMapScene, Demo]
}

export default conf