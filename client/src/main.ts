import * as phaser from 'phaser';
import conf from './conf';


export default (async function main() {
    const game = new phaser.Game(conf)
    return game
}())