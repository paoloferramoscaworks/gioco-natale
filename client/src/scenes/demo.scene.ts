import { Scene } from "phaser";
import conf from "../conf";

export default class Demo extends Scene
{
    constructor ()
    {
        super('demo');
    }

    preload ()
    {
        this.load.glsl('bundle', 'assets/plasma-bundle.glsl');
        this.load.glsl('stars', 'assets/starfields.glsl');
    }

    create ()
    {
        const shadersStarfields = [
            "Raymarched Plasma",
            "Marble",
            "Flower Plasma",
            "Plasma"
        ]
        const shadersPlasma = [
            "RGB Shift Field",
            "Layer Starfield",
            "Star Dots",
            "Retro Starfield",
            "Star Nest",
            "Star Tunnel",
            "Warp Speed",
            "Galaxy Trip",
        ]

        const MAX_HEIGHT = 256
        const MAX_WIDTH = 256
        var currentWidth = 0
        var currentHeight = 0
        var i = 0;
        const draw = (sx, sy) => {
            return current => {
                currentHeight = Math.floor(i / 4) * Math.floor(MAX_HEIGHT / sy)
                currentWidth = (i % 4) * Math.floor(MAX_WIDTH / sx)
                this.add.shader(
                    current,
                    currentWidth,
                    currentHeight,
                    Math.floor(MAX_WIDTH / sx),
                    Math.floor(MAX_HEIGHT / sy)
                ).setOrigin(0)
                i = i + 1
            }
        }
        shadersStarfields.forEach(draw(4, 3))
        shadersPlasma.forEach(draw(4, 3))

        

        this.input.on('pointerup', function (this:Scene, pointer) {
            this.scene.start('tilemap');
        }, this);

        // this.tweens.add({
        //     targets: shad,
        //     duration: 1500,
        //     ease: 'Sine.inOut',
        //     yoyo: true,
        //     repeat: -1
        // })
    }
}
