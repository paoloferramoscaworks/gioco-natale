import { Scene } from "phaser";
import floor from "./floor";
import room from "./room";


export default class TileMapScene extends Scene
{
    constructor ()
    {
        super({
            key: "tilemap"
        });
    }

    preload ()
    {
        floor.preload(this)
        room.preload(this)
    }

    create ()
    {
        floor.create(this)
        room.create(this)

        this.lights.addLight(16, 16, 128, 0xff0000, 1);
        this.lights.addLight(196, 64, 128, 0x00ff00, 1);
        this.lights.addLight(48, 196, 128, 0x0000ff, 1);

        this.input.on('pointerup', function (this:Scene, pointer) {
            this.scene.start('demo');
        }, this);
    }

    update ()
    {
        floor.update(this)
        room.update(this)
    }
}
