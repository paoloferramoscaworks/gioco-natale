import { Scene } from "phaser"

export default {
    preload(scope:Scene) {
        scope.load.image("tiles", "assets/character and tileset/Dungeon_Tileset.png")
    },

    create (scope:Scene)
    {
        const floorTileLeftOffset = 6
        const floorTileLeftMax = 10
        const floorTileTopOffset = 0
        const floorTileTopMax = 3
        const floorTiles = (() => {
            var result = []
            for(let i = floorTileTopOffset; i < floorTileTopMax; i ++) {
                for(let j = floorTileLeftOffset; j < floorTileLeftMax; j++) {
                    result.push(j + i * 10)
                }
            }
            return result
        })()
        const data = (() => {
            var result = new Array(16)
            var z = 0;
            for(let i = 0; i < 16; ++i) {
                result[i] = new Array(16)
                for(let j = 0; j < 16; ++j) {
                    result[i][j] = 78 //floorTiles[z++%floorTiles.length]
                }
            }
            return result
        })()
        const tilemap = scope.make.tilemap({
            data,
            key: "tilemap",
            tileHeight: 16,
            tileWidth: 16
        })
        tilemap.addTilesetImage("tiles")
        return tilemap.createLayer(0, "tiles", 0, 0)
    },

    update (scope:Scene)
    {
    },
}